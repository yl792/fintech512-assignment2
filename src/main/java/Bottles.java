package assignment2;

class Bottles {

	public String verse(int verseNumber) {
		if (verseNumber > 2) {
			return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
					+ "Take one down and pass it around, " + (verseNumber - 1) + " bottles of beer on the wall.\n";
		}
		else if (verseNumber == 2){
			return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
					+ "Take one down and pass it around, " + (verseNumber - 1) + " bottle of beer on the wall.\n";
		}
		else if (verseNumber == 1){
			return "1 bottle of beer on the wall, " + "1 bottle of beer.\n" + "Take it down and pass it around, "
					+ "no more bottles of beer on the wall.\n";
		}
		else{
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		}
	}

	public String verse(int startVerseNumber, int endVerseNumber) {
		String s = "";
		for (int i = startVerseNumber; i >= endVerseNumber; i--) {
			if (i > 2) {
				s = s + i + " bottles of beer on the wall, " + i + " bottles of beer.\n"
						+ "Take one down and pass it around, " + (i - 1) + " bottles of beer on the wall.\n";
			} else if (i == 2) {
				s = s + i + " bottles of beer on the wall, " + i + " bottles of beer.\n"
						+ "Take one down and pass it around, " + (i - 1) + " bottle of beer on the wall.\n";
			} else if (i == 1) {
				s = s + "1 bottle of beer on the wall, " + "1 bottle of beer.\n" + "Take it down and pass it around, "
						+ "no more bottles of beer on the wall.\n";
			} else {
				s = s + "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
						+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
			}

			if (i != endVerseNumber ){
				s = s + "\n";
			}
		}
		return s;
	}

	public String song() {
		String s = verse(99,0);
		return s;
	}
}
